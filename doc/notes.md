## Resources

- [STM32 LTDC](https://www.compel.ru/wordpress/wp-content/uploads/2013/11/1_LTDC_ChromeART.pdf)
- [STM32 LTDC Application Note](https://www.st.com/resource/en/application_note/an4861-lcdtft-display-controller-ltdc-on-stm32-mcus-stmicroelectronics.pdf)
- [STM32 SDRAM](https://www.compel.ru/wordpress/wp-content/uploads/2013/11/2_SDRAM.pdf)
- [IS42S16400J SDRAM](https://www.issi.com/ww/pdf/42-45s16400j.pdf)
- [ILI9341](https://cdn-shop.adafruit.com/datasheets/ILI9341.pdf)
- [STMPE811](https://media.digikey.com/pdf/Data%20Sheets/ST%20Microelectronics%20PDFS/STMPE811.pdf)
- [SDRAM Config Example](http://en.radzio.dxp.pl/stm32f429idiscovery/sdram.html)
- [DISCO Schematic](https://docs.rs-online.com/b72f/0900766b815247be.pdf)
- [DISCO F429 Board](https://docs.rs-online.com/b72f/0900766b815247be.pdf)
- [STM32 USB Training](https://www.youtube.com/playlist?list=PLnMKNibPkDnFFRBVD206EfnnHhQZI4Hxa)
- [STM32 USB](https://www.st.com/content/ccc/resource/training/technical/product_training/group0/de/d3/cd/3d/72/0c/4d/4e/STM32F7_Peripheral_USB_OTG_FS_HS/files/STM32F7_Peripheral_USB_OTG_FS_HS.pdf/jcr:content/translations/en.STM32F7_Peripheral_USB_OTG_FS_HS.pdf)

## Peripheral Configuration

### FMC
- Set FMC configurations:
    - Select SDRAM 1 (whose addr = 0xD0000000)
    - Add `PE1`, `PE0`, and `PG2` as FMC pins
    - Set `banks to 4`
    - Set SDCKE1+SDNE1`
    - Set `Address to 12 bits`
    - Set `Data to 16 bits`
    - Set `CAS latency to 3` (with the DISCO this doesn't matter because we can't reach more than 133MHz)
    - Set `the SDRAM common clock` to 2 HCLK clock cycles
    - Set the timing parameters:
      `Operating frequency = 120MHz / 2 = 60MHz`

    |STM Parameter       |Symbol   |Parameter                                              | -5 | -6 | -7 |Units|Formula            |Result|
    |:-------------------|:--------|:------------------------------------------------------|:---|:---|:---|:----|:------------------|:-----|
    |LoadToActiveDelay   |t_mrd    |LOAD MODE REGISTER command to ACTIVE or REFRESH command|  2 |  2 |  2 |cycle|                   |   2  |
    |ExitSelfRefreshDelay|t_xsr    |Exit Self-Refresh to Active Time                       | 60 | 66 | 70 |ns   |(1 / 60e6) * 5 > 70|   5  |
    |SelfRefreshTime     |t_ras    |Command Period (ACT to PRE)                            | 40 | 42 | 42 |ns   |(1 / 60e6) * 3 > 42|   3  |
    |RowCycleDelay       |t_rc     |Command Period (REF to REF / ACT to ACT)               | 55 | 60 | 63 |ns   |(1 / 60e6) * 4 > 63|   4  |
    |WriteRecoveryTime   |t_wr     |Input Data To Precharge or Command Delay time          |  2 |  2 |  2 |cycle|                   |   2  |
    |RPDelay             |t_rp     |Command Period (PRE to ACT)                            | 15 | 15 | 15 |ns   |(1 / 60e6) * 1 > 15|   1  |
    |RCDDelay            |t_rcd    |Active Command To Read / Write Command Delay Time      | 15 | 15 | 15 |ns   |(1 / 60e6) * 1 > 15|   1  |

### LCD
Set `PD13` and `PC2` as CS, and DC/X

### USB
- Modify the `usbd_cdc_if.c` file with these contents:
    -   ```c
        USBD_CDC_LineCodingTypeDef LineCoding = {
            115200,                       /* baud rate */
            0x00,                         /* stop bits-1 */
            0x00,                         /* parity - none */
            0x08                          /* nb. of bits 8 */
        };
        ```
    -   ```c
        case CDC_SET_LINE_CODING:
            LineCoding.bitrate = (uint32_t) (pbuf[0] | (pbuf[1] << 8) | (pbuf[2] << 16) | (pbuf[3] << 24));
            LineCoding.format = pbuf[4];
            LineCoding.paritytype = pbuf[5];
            LineCoding.datatype = pbuf[6];
            break;

        case CDC_GET_LINE_CODING:
            pbuf[0] = (uint8_t) (LineCoding.bitrate);
            pbuf[1] = (uint8_t) (LineCoding.bitrate >> 8);
            pbuf[2] = (uint8_t) (LineCoding.bitrate >> 16);
            pbuf[3] = (uint8_t) (LineCoding.bitrate >> 24);
            pbuf[4] = LineCoding.format;
            pbuf[5] = LineCoding.paritytype;
            pbuf[6] = LineCoding.datatype;
            break;
        ```
### Code Implementation
- Add the LCD / Touch / SDRAM libraries:
    - `lcd_gfx`
    - `lcd_image`
    - `lcd_ltdc`
    - `lcd_manager`
    - `sdram_manager`
    - `touch_manager`

- Add the code to the main.c:
    ```c
    #include <stdio.h>
    #include <stdint.h>
    #include <string.h>

    #include "lcd_manager.h"
    #include "lcd_ltdc.h"
    #include "sdram_manager.h"
    #include "touch_manager.h"
    #include "usbd_cdc_if.h"

    #ifdef __GNUC__
    /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
    set to 'Yes') calls __io_putchar() */
    #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
    #else
    #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
    #endif /* __GNUC__ */

    PUTCHAR_PROTOTYPE
    {
      /* Place your implementation of fputc here */
      /* e.g. write a character to the USART1 and Loop until the end of transmission */
      HAL_UART_Transmit(&huart1, (uint8_t *) &ch, 1, 0xFFFF);

      return ch;
    }

    i2c_touch_init();
    sdram_init_sequence();

    #define _USING_LCD_LTDC_    0

    #if _USING_LCD_LTDC_
      lcd_ltdc_init();
    #else
      lcd_spi_init();
      gfx_init(lcd_draw_pixel, LCD_WIDTH, LCD_HEIGHT);
      HAL_Delay(2000);
    #endif

    lcd_fill_buffers();

    uint32_t tick_layers = HAL_GetTick();
    uint32_t tick_backgorund = HAL_GetTick();
    uint32_t tick_print = HAL_GetTick();

    while (1)
    {
      if (HAL_GetTick() - tick_layers > 16)
      {
        tick_layers = HAL_GetTick();

      #if _USING_LCD_LTDC_
        lcd_ltdc_move_sprite_ly2();
        lcd_ltdc_move_sprite_ly1();
      #else
        lcd_handler_run();
      #endif
      }

      if (HAL_GetTick() - tick_backgorund > 20)
      {
        tick_backgorund = HAL_GetTick();
        HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_14);

        #if _USING_LCD_LTDC_
          lcd_ltdc_mutate_background_color();
        #endif
      }

      if (HAL_GetTick() - tick_print > 500)
      {
        tick_print = HAL_GetTick();
        HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_13);

        char *buff = "hallo\r\n";
        CDC_Transmit_HS((uint8_t *) buff, (uint16_t) strlen(buff));
        // sdram_handler();
      }

      i2c_touch_handler();
    }

    ```

-------------------------------------------------------------------------------------